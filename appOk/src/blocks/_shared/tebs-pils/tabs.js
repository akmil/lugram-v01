import {CONST} from '../../../common/js-services/consts';
import User from '../../../common/js-services/user';

const fillDropdownUsersCfg = {};

function fillDropdownUsers({$wrapper, logsState, onChangeSelectCb}, accounts) {
    const {selectCls} = logsState;
    const label = 'Выберите аккаунт';
    $wrapper.empty().addClass('border-light-color');
    $(`<div class="">${label}</div><select name="task-type" class="${selectCls}"></select>`).appendTo($wrapper);
    accounts.forEach((name) => {
        $(`<option class="list-group-item py-2" value="${name}">
            ${name}
        </option>`).appendTo($(`.${selectCls}`));
    });
    onChangeSelectCb();
}

function getMetaLazy() {
    User.getMetadataLazy().then((res) => {
        if (res.status.state === 'ok' && res.data && res.data.accounts) {
            const cfg = fillDropdownUsersCfg;
            const {accounts} = res.data;
            fillDropdownUsers(cfg, accounts);
            window.PubSub.publish(CONST.events.instagramAccouns.INSTAGRAM_ACCOUNS_RENDERED_LAZY, res.data.accounts);
        }
    });
}

function tabHandler(/* isComputeMultyDropdown*/) {
    $('#v-pills-logs-tab').on('click', () => {
        getMetaLazy();
    });
}

export function init(onChangeSelect, logsState) {
    const $wrapper = $('.log-users-list');
    Object.assign(fillDropdownUsersCfg, {$wrapper, onChangeSelectCb: onChangeSelect, logsState});
    tabHandler();
}
